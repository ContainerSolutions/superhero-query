with import <nixpkgs> {};
let
 unstable = import <nixos-unstable> {};
in
stdenv.mkDerivation rec {
  name = "env";
  env = buildEnv { name = name; paths = buildInputs; };
  buildInputs = [
    python36Packages.virtualenv
  ];
}
